package com.example.appecommerce.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.appecommerce.Adapter.PopularAdapter;
import com.example.appecommerce.Domain.PopularDomain;
import com.example.appecommerce.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView.Adapter adapterPopular;
    private RecyclerView recyclerViewPopular;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerView();

        bottomNavigation();
    }

    private void bottomNavigation() {
        LinearLayout homeBtn=findViewById(R.id.homeBtn);
        LinearLayout cartBtn=findViewById(R.id.cartBtn);

        homeBtn.setOnClickListener(view -> startActivity(new Intent(MainActivity.this,MainActivity.class)));
        cartBtn.setOnClickListener(view -> startActivity(new Intent(MainActivity.this,CartActivity.class)));
    }

    private void initRecyclerView() {
        ArrayList<PopularDomain> items = new ArrayList<>();
        items.add(new PopularDomain("T-shirt black","Immerse yourself in a world of vibrant visual and immersive sound with the VisionX Pro LED TV. Its cutting-edge LED technology brings every scene to life with striking clarity and rich colors. With seamless integration and a sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for your entertainment space. With its sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for you entertainment space. The ultra-slime bezel and premium finish blend seamlessly with any decor","item_1",15,4,50));
        items.add(new PopularDomain("Smart Watch","Immerse yourself in a world of vibrant visual and immersive sound with the VisionX Pro LED TV. Its cutting-edge LED technology brings every scene to life with striking clarity and rich colors. With seamless integration and a sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for your entertainment space. With its sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for you entertainment space. The ultra-slime bezel and premium finish blend seamlessly with any decor","item_2",10,4.5,250));
        items.add(new PopularDomain("IPhone14","Immerse yourself in a world of vibrant visual and immersive sound with the VisionX Pro LED TV. Its cutting-edge LED technology brings every scene to life with striking clarity and rich colors. With seamless integration and a sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for your entertainment space. With its sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for you entertainment space. The ultra-slime bezel and premium finish blend seamlessly with any decor","item_3",15,4.3,800));
        items.add(new PopularDomain("VisionX Pro LED Tv","Immerse yourself in a world of vibrant visual and immersive sound with the VisionX Pro LED TV. Its cutting-edge LED technology brings every scene to life with striking clarity and rich colors. With seamless integration and a sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for your entertainment space. With its sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for you entertainment space. The ultra-slime bezel and premium finish blend seamlessly with any decor","item_4",18,4.0,999));
        items.add(new PopularDomain("Nike In-Season TR 13","Immerse yourself in a world of vibrant visual and immersive sound with the VisionX Pro LED TV. Its cutting-edge LED technology brings every scene to life with striking clarity and rich colors. With seamless integration and a sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for your entertainment space. With its sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for you entertainment space. The ultra-slime bezel and premium finish blend seamlessly with any decor","item_5",17,4.5,399));
        items.add(new PopularDomain("Exercise Equipment","Immerse yourself in a world of vibrant visual and immersive sound with the VisionX Pro LED TV. Its cutting-edge LED technology brings every scene to life with striking clarity and rich colors. With seamless integration and a sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for your entertainment space. With its sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for you entertainment space. The ultra-slime bezel and premium finish blend seamlessly with any decor","item_6",16,4.4,299));
        items.add(new PopularDomain("ZNH Mountain Electric Bicycle,Black","Immerse yourself in a world of vibrant visual and immersive sound with the VisionX Pro LED TV. Its cutting-edge LED technology brings every scene to life with striking clarity and rich colors. With seamless integration and a sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for your entertainment space. With its sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for you entertainment space. The ultra-slime bezel and premium finish blend seamlessly with any decor","item_7",18,4.5,1399));
        items.add(new PopularDomain("MacBook Pro featuring M2 Pro and M2 Max - Apple","Immerse yourself in a world of vibrant visual and immersive sound with the VisionX Pro LED TV. Its cutting-edge LED technology brings every scene to life with striking clarity and rich colors. With seamless integration and a sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for your entertainment space. With its sleek, modern design, the VisionX Pro is not just a TV, but a centrepiece for you entertainment space. The ultra-slime bezel and premium finish blend seamlessly with any decor","item_8",18,4.5,2999));
        recyclerViewPopular=findViewById(R.id.view1);
        recyclerViewPopular.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        adapterPopular=new PopularAdapter(items);
        recyclerViewPopular.setAdapter(adapterPopular);

        System.out.println("HelloWold");

    }
}